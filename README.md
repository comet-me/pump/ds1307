Q. Why don't you use the rtc-ds1307 overlay?
A. I didn't manage to install it, writing this was faster (at least I want to believe that).

Q. What does this project do?
A. Implement enough of the ds1307 I2C protocol to enable reading and writing time, and write it to the system after boot.

Hardware & Software I'm using:
- RPi 3 version B (1.2)
- 64 bit debian buster image https://wiki.debian.org/RaspberryPi3

Common solution is:
- enable i2c
- enable dtoverlay via /boot/config.txt for i2c-rtc,ds1307

The first stage works via i2c-dev module, the second fails (the second should bring in the driver module automatically, so it should be the only one needed)

This project is a workaround:
Instead of a /dev/rtc0 device being generated and used automatically by the system (via systemd-timesync? no idea), this code runs as a oneshot systemd service at startup,
reading the ds1307 and writing to the system (./ds1307.py --hctosys).

Manually I initialized the device once before (./ds1307.py --systohc)
