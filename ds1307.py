#!/usr/bin/python3


from argparse import ArgumentParser
import time
from datetime import datetime
from subprocess import check_call


import smbus


DS1307_I2C_ADDRESS = 0x68
SECONDS_REGISTER = 0
MINUTES_REGISTER = 1
HOUR_REGISTER = 2
WDAY_REGISTER = 3 # 1 - 7
MDAY_REGISTER = 4 # 1 - 31
MONTH_REGISTER = 5 # 1 - 12
YEAR_REGISTER = 6 # 00 - 99


def bin2bcd(b):
    tens = b // 10
    singles = b - tens * 10
    return (singles) | (tens << 4)


def bcd2bin(dec):
    return (dec & 0xf) + (dec >> 4) * 10


def vec_bin2bcd(v):
    return [bin2bcd(x) for x in v]


def vec_bcd2bin(v):
    return [bcd2bin(x) for x in v]


class DS1307:
    """
    RPi Pinouts

    I2C Pins 
    GPIO2 -> SDA
    GPIO3 -> SCL
    """
    def __init__(self):
        # for RPI version 1, use "bus = smbus.SMBus(0)"
        self.bus = bus = smbus.SMBus(1)


    def read_byte(self, offset):
        return self.bus.read_byte_data(DS1307_I2C_ADDRESS, offset)


    def read_block(self, offset, size):
        return self.bus.read_i2c_block_data(DS1307_I2C_ADDRESS, offset, size)


    def write_block(self, offset, data):
        self.bus.write_i2c_block_data(DS1307_I2C_ADDRESS, offset, data)


    def read_dec_byte(self, offset):
        return bcd2bin(self.read_byte(offset))


    def read_seconds(self):
        return self.read_dec_byte(SECONDS_REGISTER)


    def read_minutes(self):
        return self.read_dec_byte(MINUTES_REGISTER)


    def read_hour(self):
        return self.read_dec_byte(HOUR_REGISTER)


    def read_time(self):
        return vec_bcd2bin(reversed(self.read_block(SECONDS_REGISTER, 3)))
        #return self.read_hour(), self.read_minutes(), self.read_seconds()


    def write_time(self, hour, minute, second):
        self.write_block(SECONDS_REGISTER, vec_bin2bcd((second, minute, hour)))


    def read_date(self):
        mday, month, year = vec_bcd2bin(self.read_block(MDAY_REGISTER, 3))
        year += 2000
        return 

    def write_datetime(self, dt):
        year = dt.year - 2000
        month = dt.month
        mday = dt.day
        wday = ((dt.weekday() + 1) % 7) + 1 # Datetime: Monday is 0, Sunday is 6 ; DS1307: Sunday is 1 -> Saturday is 7
        hour = dt.hour
        minute = dt.minute
        second = dt.second
        self.write_block(SECONDS_REGISTER, vec_bin2bcd((second, minute, hour, wday, mday, month, year)))


    def read_datetime(self):
        second, minute, hour, wday, mday, month, year = vec_bcd2bin(self.read_block(SECONDS_REGISTER, 7))
        dt = datetime(
                year=year + 2000,
                month=month,
                day=mday,
                hour=hour,
                minute=minute,
                second=second,
                )
        expected_wday = ((dt.weekday() + 1) % 7) + 1
        if expected_wday != wday:
            print(f"inconsistent weekday: calculated {expected_wday}, got {wday}")
        return dt


    def endless_show(self):
        while True:
            h, m, s = self.read_time()
            print(f'{h:02}:{m:02}:{s:02}', end='\r')
            time.sleep(0.5)


def sys_set_datetime(dt):
    check_call(['date', '-s', str(dt)])


def main():
    parser = ArgumentParser(description='DS1307 module time read/write via I2C on a RPi 3')
    parser.add_argument('--set-ds')
    parser.add_argument('--hctosys', default=False, action='store_true', help='write DS1307 to system')
    parser.add_argument('--dryrun', default=False, action='store_true', help='do not change system')
    parser.add_argument('--systohc', default=False, action='store_true', help='write system to DS1307')
    args = parser.parse_args()
    if sum([args.set_ds != None, args.hctosys, args.systohc]) > 1:
        print('cannot set more than one flag')
        return

    clock = DS1307()
    if args.set_ds:
        h, m, s = [int(x) for x in args.set_ds.split(':')]
        clock.write_time(hour=h, minute=m, second=s)
    elif args.hctosys:
        clock_dt = clock.read_datetime()
        if args.dryrun:
            print(f"dryrun: clock {clock_dt}")
            total_seconds = (clock_dt - datetime.now()).total_seconds()
            print(f"dryrun: ahead of system: {total_seconds}")
        else:
            sys_set_datetime(clock_dt)
    elif args.systohc:
        now = datetime.now()
        if not args.dryrun:
            clock.write_datetime(now)
        else:
            print(f"dryrun: clock ahead of system: {(clock.read_datetime() - now).total_seconds()}")
    else:
        clock.endless_show()


if __name__ == '__main__':
    main()

